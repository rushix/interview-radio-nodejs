export default function(caseChecks = []) {
  if (!Array.isArray(caseChecks)) {
    throw new TypeError('param should be an array');
  }

  const failingCases = caseChecks.filter(caseCheck => {
    if (!Array.isArray(caseCheck)) {
      throw new TypeError('param should be an array of arrays');
    }

    const [cas, err] = caseCheck;
    if (typeof cas !== 'boolean') {
      throw new TypeError('case should be a boolean');
    }

    if (!(err instanceof Error)) {
      throw new TypeError('err should be instance of Error class');
    }

    return !cas;
  });

  failingCases.forEach(caseCheck => {
    const [, err] = caseCheck;

    throw err;
  });
}
