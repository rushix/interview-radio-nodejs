import iFetch from 'isomorphic-fetch';
import { parseString } from 'xml2js';
import { promisify, Promise } from 'bluebird';

import config from './config';
import Collection from './Song/Collection';

process.on('unhandledRejection', err => {
  console.error(`handle unhandledRejection: ${ err }`);
});

const { songsCollectionSourceDefault } = config;

export function playlistBySongsCount(count, source = songsCollectionSourceDefault) {
  getSongsList(source)
    .then(songsList => {
      try {
        const songsCollection = new Collection(songsList);
        songsCollection.outputPlaylistBySongsCount(count);
      } catch(err) {
        console.error('Error: ', err);
      }
    })
    .catch(err => {
      throw new Error(`source parsing error: ${ err }`, err);
    });
}

export function playlistByDuration(duration, source = songsCollectionSourceDefault) {
  getSongsList(source)
    .then(songsList => {
      try {
        const songsCollection = new Collection(songsList);
        songsCollection.outputPlaylistByDuration(duration);
      } catch(err) {
        console.error('Error: ', err);
      }
    })
    .catch(err => {
      throw new Error(`source parsing error: ${ err }`, err);
    });
}

function getSongsList(songsCollectionSource) {
  const parseStringPromise = promisify(parseString);
  const parseStringOptions = {
    normalizeTags: true,
    explicitRoot: false
  };

  return iFetch(songsCollectionSource)
    .then(songsCollectionRaw => songsCollectionRaw.text())
    .then(songsCollectionXML => parseStringPromise(songsCollectionXML, parseStringOptions))
    .catch(err => {
      throw new Error(`source parsing error: ${ err }`);
    });
}
