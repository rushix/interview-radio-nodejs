import thrower from '../util/thrower';
import Vertices from './Vertices';

export default class {
  constructor(name, duration, id) {
    const trimmedName = name.toString().toLowerCase().replace(/[^a-z0-9]/g, '');

    thrower([
      [typeof duration === 'number', new TypeError('duration should be a number')],
      [Number.isInteger(duration), new TypeError('duration should be an integer')],
      [duration > 0, new TypeError('duration length should be more then 0')],
      [typeof name === 'string', new TypeError('name should be a string')],
      [trimmedName.length > 0, new TypeError('name format unsupported')]
    ]);

    this.name = name;
    this.duration = duration;
    this.id = id;

    this.vertices = new Vertices(trimmedName);

    Object.freeze(this);
  }
}
