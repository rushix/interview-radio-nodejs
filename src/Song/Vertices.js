import thrower from '../util/thrower';

export default class {
  constructor(trimmedName) {
    thrower([
      [typeof trimmedName === 'string', new TypeError('trimmedName should be string')],
      [trimmedName.length > 0, new TypeError('trimmedName\'s length should be greater than 0')],
      [trimmedName.match(/[^a-z0-9]/gi) === null, new Error('trimmedName has unsupported char')]
    ]);

    const firstChar = trimmedName.slice(0, 1);
    const lastChar = trimmedName.slice(-1);

    const [from, to] = [firstChar, lastChar].map(c => {
      const code = c.charCodeAt(0);

      switch (true) {
        case code > 47 && code < 58:
          return code - 48;
        default:
          return code - 87;
      }
    });

    this.pair = { from, to };
    this.tag = JSON.stringify(this.pair);

    Object.freeze(this.pair);
    Object.freeze(this);
  }
}
