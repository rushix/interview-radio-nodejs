import thrower from '../util/thrower';
import Song from '../Song';
import Graph from '../Graph';

export default class {
  constructor(songsBar) {
    const graphDimension = 36;
    const { artist: artists } = songsBar;

    thrower([
      [artists !== void 0 && Array.isArray(artists), new TypeError('source has wrong format')]
    ]);

    this.songsList = artists.reduce((songsListAcc, artist) => {
      const { song: songs } = artist;

      thrower([
        [songs !== void 0 && Array.isArray(songs), new TypeError('source has wrong format')]
      ]);

      // hello "=:0 :(" :-)
      const filteredSongs = songs.filter(song => {
        const { name } = song.$;

        return name.toLowerCase().replace(/[^a-z0-9]/gi, '').length > 1;
      });

      const songsOfArtist = filteredSongs.map(song => {
        const { name, duration, id } = song.$;

        return new Song(name, parseInt(duration, 10), id);
      });

      return [...songsListAcc, ...songsOfArtist];
    }, []);

    thrower([
      [this.songsList.length !== 0, new Error('songs list is empty')]
    ]);

    this.collection = new Graph(graphDimension);
    this.songsList.forEach(song => {
      const { from, to } = song.vertices.pair;

      this.collection.addEdge(from, to, song.duration, song);
    });
  }

  getRandomSong() {
    const maxSongIndex = this.songsList.length - 1;
    const randomSongIndex = parseInt(Math.random() * maxSongIndex, 10);

    return this.songsList[randomSongIndex];
  }

  outputPlaylist(prefix, firstSong, songsList, lastSong) {
    let counter = 0;

    console.log(`${ prefix }\n`);

    if (firstSong) {
      console.log(`${ ++counter }. ${ firstSong.name }\n`);
    }

    if (songsList) {
      songsList.forEach(song => {
        console.log(`${ ++counter }. ${ song.name }\n`);
      });
    }

    if (lastSong) {
      console.log(`${ ++counter }. ${ lastSong.name }\n`);
    }
  }

  outputShortestPlaylist(firstSong, lastSong) {
    const { to: firstTo } = firstSong.vertices.pair;
    const { from: lastFrom } = lastSong.vertices.pair;

    if (firstTo === lastFrom) {
      this.outputPlaylist('Shortest playlist: ', firstSong, null, lastSong);
    } else {
      const shortestPlaylist = this.collection.findShortestWay(firstTo, lastFrom);

      if (!shortestPlaylist.found) {
        return this.outputList('Shortest playlist is not found');
      }

      const shortestSongsList = shortestPlaylist.path.map(p => p.data);
      this.outputPlaylist('Shortest playlist: ', firstSong, shortestSongsList, lastSong);
    }
  }

  outputPlaylistBySongsCount(count) {
    const firstSong = this.getRandomSong();
    const { to: firstTo } = firstSong.vertices.pair;

    if (count === 1) {
      this.outputPlaylist('Playlist: ', firstSong);
    } else {
      const playlist = this.collection.dfsByEdgesNumber(firstTo, count - 1);

      if (!playlist.found) {
        return this.outputPlaylist('Playlist is not found');
      }

      const songsList = playlist.path.map(p => p.data);
      this.outputPlaylist('Playlist: ', firstSong, songsList);

      const lastSong = songsList.pop();
      this.outputShortestPlaylist(firstSong, lastSong);
    }
  }

  outputPlaylistByDuration(duration) {
    const firstSong = this.getRandomSong();
    const { to: firstTo } = firstSong.vertices.pair;

    if (duration < firstSong.duration) {
      return this.outputPlaylist('Playlist is not found');
    } else if (duration === firstSong.duration) {
      this.outputPlaylist('Playlist: ', firstSong);
    } else {
      const playlist = this.collection.dfsByWeight(firstTo, duration - firstSong.duration);

      if (!playlist.found) {
        return this.outputPlaylist('Playlist is not found');
      }

      const songsList = playlist.path.map(p => p.data);
      this.outputPlaylist('Playlist: ', firstSong, songsList);

      const lastSong = songsList.pop();
      this.outputShortestPlaylist(firstSong, lastSong);
    }
  }
}
