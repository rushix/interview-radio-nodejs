import thrower from '../util/thrower';
import Node from './Node';
import jsgraphs from 'js-graph-algorithms';

export default class {
  constructor(length) {

    thrower([
      [typeof length === 'number', new TypeError('length should be number')],
      [Number.isInteger(length), new TypeError('length should be integer')],
      [length > 0, new Error('length should be greater than 0')]
    ]);

    this.length = length;

    const innerBar = () => Array.from({ length }, () => new Node());
    this.graph = Array.from({ length }, () => innerBar());
  }

  addEdge(from, to, weight, data) {
    thrower([
      [typeof from === 'number', new TypeError('from should be number')],
      [Number.isInteger(from), new TypeError('from should be integer')],
      [from >= 0 && from < this.length, new RangeError('from is out of range')],

      [typeof to === 'number', new TypeError('to should be number')],
      [Number.isInteger(to), new TypeError('to should be integer')],
      [to >= 0 && to < this.length, new RangeError('to is out of range')]
    ]);

    this.graph[from][to].addEdge(weight, data);
  }

  dfsByEdgesNumber(from, edgesNumber) {
    thrower([
      [typeof from === 'number', new TypeError('from should be number')],
      [Number.isInteger(from), new TypeError('from should be integer')],
      [from >= 0 && from < this.length, new RangeError('from is out of range')],

      [typeof edgesNumber === 'number', new TypeError('edgesNumber should be number')],
      [Number.isInteger(edgesNumber), new TypeError('edgesNumber should be integer')],
      [edgesNumber > 0, new RangeError('edgesNumber is out of range')]
    ]);

    const result = {
      found: false,
      path: []
    };

    const dfs = (currentVertex, path) => {
      if (path.length === edgesNumber) {
        result.found = true;
        result.path = path;
      }

      if (result.found) {
        return true;
      }

      this.graph[currentVertex].forEach((vertex, vertexIndex) => {
        if (vertex.hasEdges()) {
          dfs(vertexIndex, [...path, vertex.anyEdge()]);
        }
      });
    };

    dfs(from, []);

    return result;
  }

  dfsByWeight(from, weight) {
    thrower([
      [typeof from === 'number', new TypeError('from should be number')],
      [Number.isInteger(from), new TypeError('from should be integer')],
      [from >= 0 && from < this.length, new RangeError('from is out of range')],

      [typeof weight === 'number', new TypeError('weight should be number')],
      [Number.isInteger(weight), new TypeError('weight should be integer')],
      [weight > 0, new RangeError('weight is out of range')]
    ]);

    const result = {
      found: false,
      path: []
    };

    const dfs = (currentVertex, path) => {
      const pathWeight = path.reduce((weightAcc, p) => weightAcc + p.weight, 0);

      if (pathWeight === weight) {
        result.found = true;
        result.path = path;
      }

      if (result.found) {
        return;
      }

      this.graph[currentVertex].forEach((vertex, vertexIndex) => {
        if (vertex.hasEdges()) {
          vertex.edges.forEach(edge => {
            if (edge.weight + pathWeight <= weight) {
              dfs(vertexIndex, [...path, edge]);
            }
          });
        }
      });
    };

    dfs(from, []);

    return result;
  }

  findShortestWay(from, to) {
    thrower([
      [typeof from === 'number', new TypeError('from should be number')],
      [Number.isInteger(from), new TypeError('from should be integer')],
      [from >= 0 && from < this.length, new RangeError('from is out of range')],

      [typeof to === 'number', new TypeError('to should be number')],
      [Number.isInteger(to), new TypeError('to should be integer')],
      [to >= 0 && to < this.length, new RangeError('to is out of range')],
    ]);

    const vGraph = new jsgraphs.WeightedDiGraph(this.length);

    this.graph.forEach((vertices, x) => {
      vertices.forEach((vertex, y) => {
        if (vertex.hasEdges()) {
          const shortestEdge = vertex.shortestEdge();

          vGraph.addEdge(new jsgraphs.Edge(x, y, shortestEdge.weight));
        }
      });
    });

    const dijkstra = new jsgraphs.Dijkstra(vGraph, from);

    const result = {
      found: false,
      path: []
    };

    if (dijkstra.hasPathTo(to)) {
      const shortestPath = dijkstra.pathTo(to).map(e => {
        const { v, w } = e;

        return this.graph[v][w].shortestEdge();
      });

      result.found = true;
      result.path = shortestPath;
    }

    return result;
  }
}
