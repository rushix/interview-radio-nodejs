import thrower from '../util/thrower';

export default class {
  constructor() {
    this.edges = [];
  }

  addEdge(weight, data) {
    thrower([
      [typeof weight === 'number', new TypeError('weight should be number')],
      [Number.isInteger(weight), new TypeError('weight should be integer')],
      [weight > 0, new Error('weight should be greater than 0')]
    ]);

    this.edges.push({ weight, data });
  }

  shortestEdge() {
    thrower([
      [this.edges.length > 0, new Error('this Node has no edges')]
    ]);

    return this.edges.reduce((currentEdge, nextEdge) => {
      return currentEdge.weight < nextEdge.weight ? currentEdge : nextEdge;
    });
  }

  anyEdge() {
    thrower([
      [this.edges.length > 0, new Error('this Node has no edges')]
    ]);

    const randomIndex = parseInt(Math.random() * (this.edges.length - 1), 10);

    return this.edges[randomIndex];
  }

  hasEdges() {
    return this.edges.length > 0;
  }
}
