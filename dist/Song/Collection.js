'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _thrower = require('../util/thrower');

var _thrower2 = _interopRequireDefault(_thrower);

var _Song = require('../Song');

var _Song2 = _interopRequireDefault(_Song);

var _Graph = require('../Graph');

var _Graph2 = _interopRequireDefault(_Graph);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var _class = function () {
  function _class(songsBar) {
    var _this = this;

    _classCallCheck(this, _class);

    var graphDimension = 36;
    var artists = songsBar.artist;


    (0, _thrower2.default)([[artists !== void 0 && Array.isArray(artists), new TypeError('source has wrong format')]]);

    this.songsList = artists.reduce(function (songsListAcc, artist) {
      var songs = artist.song;


      (0, _thrower2.default)([[songs !== void 0 && Array.isArray(songs), new TypeError('source has wrong format')]]);

      // hello "=:0 :(" :-)
      var filteredSongs = songs.filter(function (song) {
        var name = song.$.name;


        return name.toLowerCase().replace(/[^a-z0-9]/gi, '').length > 1;
      });

      var songsOfArtist = filteredSongs.map(function (song) {
        var _song$$ = song.$,
            name = _song$$.name,
            duration = _song$$.duration,
            id = _song$$.id;


        return new _Song2.default(name, parseInt(duration, 10), id);
      });

      return [].concat(_toConsumableArray(songsListAcc), _toConsumableArray(songsOfArtist));
    }, []);

    (0, _thrower2.default)([[this.songsList.length !== 0, new Error('songs list is empty')]]);

    this.collection = new _Graph2.default(graphDimension);
    this.songsList.forEach(function (song) {
      var _song$vertices$pair = song.vertices.pair,
          from = _song$vertices$pair.from,
          to = _song$vertices$pair.to;


      _this.collection.addEdge(from, to, song.duration, song);
    });
  }

  _createClass(_class, [{
    key: 'getRandomSong',
    value: function getRandomSong() {
      var maxSongIndex = this.songsList.length - 1;
      var randomSongIndex = parseInt(Math.random() * maxSongIndex, 10);

      return this.songsList[randomSongIndex];
    }
  }, {
    key: 'outputPlaylist',
    value: function outputPlaylist(prefix, firstSong, songsList, lastSong) {
      var counter = 0;

      console.log(prefix + '\n');

      if (firstSong) {
        console.log(++counter + '. ' + firstSong.name + '\n');
      }

      if (songsList) {
        songsList.forEach(function (song) {
          console.log(++counter + '. ' + song.name + '\n');
        });
      }

      if (lastSong) {
        console.log(++counter + '. ' + lastSong.name + '\n');
      }
    }
  }, {
    key: 'outputShortestPlaylist',
    value: function outputShortestPlaylist(firstSong, lastSong) {
      var firstTo = firstSong.vertices.pair.to;
      var lastFrom = lastSong.vertices.pair.from;


      if (firstTo === lastFrom) {
        this.outputPlaylist('Shortest playlist: ', firstSong, null, lastSong);
      } else {
        var shortestPlaylist = this.collection.findShortestWay(firstTo, lastFrom);

        if (!shortestPlaylist.found) {
          return this.outputList('Shortest playlist is not found');
        }

        var shortestSongsList = shortestPlaylist.path.map(function (p) {
          return p.data;
        });
        this.outputPlaylist('Shortest playlist: ', firstSong, shortestSongsList, lastSong);
      }
    }
  }, {
    key: 'outputPlaylistBySongsCount',
    value: function outputPlaylistBySongsCount(count) {
      var firstSong = this.getRandomSong();
      var firstTo = firstSong.vertices.pair.to;


      if (count === 1) {
        this.outputPlaylist('Playlist: ', firstSong);
      } else {
        var playlist = this.collection.dfsByEdgesNumber(firstTo, count - 1);

        if (!playlist.found) {
          return this.outputPlaylist('Playlist is not found');
        }

        var songsList = playlist.path.map(function (p) {
          return p.data;
        });
        this.outputPlaylist('Playlist: ', firstSong, songsList);

        var lastSong = songsList.pop();
        this.outputShortestPlaylist(firstSong, lastSong);
      }
    }
  }, {
    key: 'outputPlaylistByDuration',
    value: function outputPlaylistByDuration(duration) {
      var firstSong = this.getRandomSong();
      var firstTo = firstSong.vertices.pair.to;


      if (duration < firstSong.duration) {
        return this.outputPlaylist('Playlist is not found');
      } else if (duration === firstSong.duration) {
        this.outputPlaylist('Playlist: ', firstSong);
      } else {
        var playlist = this.collection.dfsByWeight(firstTo, duration - firstSong.duration);

        if (!playlist.found) {
          return this.outputPlaylist('Playlist is not found');
        }

        var songsList = playlist.path.map(function (p) {
          return p.data;
        });
        this.outputPlaylist('Playlist: ', firstSong, songsList);

        var lastSong = songsList.pop();
        this.outputShortestPlaylist(firstSong, lastSong);
      }
    }
  }]);

  return _class;
}();

exports.default = _class;