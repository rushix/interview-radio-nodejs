'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _thrower = require('../util/thrower');

var _thrower2 = _interopRequireDefault(_thrower);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var _class = function _class(trimmedName) {
  _classCallCheck(this, _class);

  (0, _thrower2.default)([[typeof trimmedName === 'string', new TypeError('trimmedName should be string')], [trimmedName.length > 0, new TypeError('trimmedName\'s length should be greater than 0')], [trimmedName.match(/[^a-z0-9]/gi) === null, new Error('trimmedName has unsupported char')]]);

  var firstChar = trimmedName.slice(0, 1);
  var lastChar = trimmedName.slice(-1);

  var _map = [firstChar, lastChar].map(function (c) {
    var code = c.charCodeAt(0);

    switch (true) {
      case code > 47 && code < 58:
        return code - 48;
      default:
        return code - 87;
    }
  }),
      _map2 = _slicedToArray(_map, 2),
      from = _map2[0],
      to = _map2[1];

  this.pair = { from: from, to: to };
  this.tag = JSON.stringify(this.pair);

  Object.freeze(this.pair);
  Object.freeze(this);
};

exports.default = _class;