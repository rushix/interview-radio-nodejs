'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _thrower = require('../util/thrower');

var _thrower2 = _interopRequireDefault(_thrower);

var _Vertices = require('./Vertices');

var _Vertices2 = _interopRequireDefault(_Vertices);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var _class = function _class(name, duration, id) {
  _classCallCheck(this, _class);

  var trimmedName = name.toString().toLowerCase().replace(/[^a-z0-9]/g, '');

  (0, _thrower2.default)([[typeof duration === 'number', new TypeError('duration should be a number')], [Number.isInteger(duration), new TypeError('duration should be an integer')], [duration > 0, new TypeError('duration length should be more then 0')], [typeof name === 'string', new TypeError('name should be a string')], [trimmedName.length > 0, new TypeError('name format unsupported')]]);

  this.name = name;
  this.duration = duration;
  this.id = id;

  this.vertices = new _Vertices2.default(trimmedName);

  Object.freeze(this);
};

exports.default = _class;