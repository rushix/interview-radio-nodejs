'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.playlistBySongsCount = playlistBySongsCount;
exports.playlistByDuration = playlistByDuration;

var _isomorphicFetch = require('isomorphic-fetch');

var _isomorphicFetch2 = _interopRequireDefault(_isomorphicFetch);

var _xml2js = require('xml2js');

var _bluebird = require('bluebird');

var _config = require('./config');

var _config2 = _interopRequireDefault(_config);

var _Collection = require('./Song/Collection');

var _Collection2 = _interopRequireDefault(_Collection);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

process.on('unhandledRejection', function (err) {
  console.error('handle unhandledRejection: ' + err);
});

var songsCollectionSourceDefault = _config2.default.songsCollectionSourceDefault;
function playlistBySongsCount(count) {
  var source = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : songsCollectionSourceDefault;

  getSongsList(source).then(function (songsList) {
    try {
      var songsCollection = new _Collection2.default(songsList);
      songsCollection.outputPlaylistBySongsCount(count);
    } catch (err) {
      console.error('Error: ', err);
    }
  }).catch(function (err) {
    throw new Error('source parsing error: ' + err, err);
  });
}

function playlistByDuration(duration) {
  var source = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : songsCollectionSourceDefault;

  getSongsList(source).then(function (songsList) {
    try {
      var songsCollection = new _Collection2.default(songsList);
      songsCollection.outputPlaylistByDuration(duration);
    } catch (err) {
      console.error('Error: ', err);
    }
  }).catch(function (err) {
    throw new Error('source parsing error: ' + err, err);
  });
}

function getSongsList(songsCollectionSource) {
  var parseStringPromise = (0, _bluebird.promisify)(_xml2js.parseString);
  var parseStringOptions = {
    normalizeTags: true,
    explicitRoot: false
  };

  return (0, _isomorphicFetch2.default)(songsCollectionSource).then(function (songsCollectionRaw) {
    return songsCollectionRaw.text();
  }).then(function (songsCollectionXML) {
    return parseStringPromise(songsCollectionXML, parseStringOptions);
  }).catch(function (err) {
    throw new Error('source parsing error: ' + err);
  });
}