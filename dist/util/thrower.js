'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

exports.default = function () {
  var caseChecks = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];

  if (!Array.isArray(caseChecks)) {
    throw new TypeError('param should be an array');
  }

  var failingCases = caseChecks.filter(function (caseCheck) {
    if (!Array.isArray(caseCheck)) {
      throw new TypeError('param should be an array of arrays');
    }

    var _caseCheck = _slicedToArray(caseCheck, 2),
        cas = _caseCheck[0],
        err = _caseCheck[1];

    if (typeof cas !== 'boolean') {
      throw new TypeError('case should be a boolean');
    }

    if (!(err instanceof Error)) {
      throw new TypeError('err should be instance of Error class');
    }

    return !cas;
  });

  failingCases.forEach(function (caseCheck) {
    var _caseCheck2 = _slicedToArray(caseCheck, 2),
        err = _caseCheck2[1];

    throw err;
  });
};