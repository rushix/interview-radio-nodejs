'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _thrower = require('../util/thrower');

var _thrower2 = _interopRequireDefault(_thrower);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var _class = function () {
  function _class() {
    _classCallCheck(this, _class);

    this.edges = [];
  }

  _createClass(_class, [{
    key: 'addEdge',
    value: function addEdge(weight, data) {
      (0, _thrower2.default)([[typeof weight === 'number', new TypeError('weight should be number')], [Number.isInteger(weight), new TypeError('weight should be integer')], [weight > 0, new Error('weight should be greater than 0')]]);

      this.edges.push({ weight: weight, data: data });
    }
  }, {
    key: 'shortestEdge',
    value: function shortestEdge() {
      (0, _thrower2.default)([[this.edges.length > 0, new Error('this Node has no edges')]]);

      return this.edges.reduce(function (currentEdge, nextEdge) {
        return currentEdge.weight < nextEdge.weight ? currentEdge : nextEdge;
      });
    }
  }, {
    key: 'anyEdge',
    value: function anyEdge() {
      (0, _thrower2.default)([[this.edges.length > 0, new Error('this Node has no edges')]]);

      var randomIndex = parseInt(Math.random() * (this.edges.length - 1), 10);

      return this.edges[randomIndex];
    }
  }, {
    key: 'hasEdges',
    value: function hasEdges() {
      return this.edges.length > 0;
    }
  }]);

  return _class;
}();

exports.default = _class;