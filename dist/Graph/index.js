'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _thrower = require('../util/thrower');

var _thrower2 = _interopRequireDefault(_thrower);

var _Node = require('./Node');

var _Node2 = _interopRequireDefault(_Node);

var _jsGraphAlgorithms = require('js-graph-algorithms');

var _jsGraphAlgorithms2 = _interopRequireDefault(_jsGraphAlgorithms);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var _class = function () {
  function _class(length) {
    _classCallCheck(this, _class);

    (0, _thrower2.default)([[typeof length === 'number', new TypeError('length should be number')], [Number.isInteger(length), new TypeError('length should be integer')], [length > 0, new Error('length should be greater than 0')]]);

    this.length = length;

    var innerBar = function innerBar() {
      return Array.from({ length: length }, function () {
        return new _Node2.default();
      });
    };
    this.graph = Array.from({ length: length }, function () {
      return innerBar();
    });
  }

  _createClass(_class, [{
    key: 'addEdge',
    value: function addEdge(from, to, weight, data) {
      (0, _thrower2.default)([[typeof from === 'number', new TypeError('from should be number')], [Number.isInteger(from), new TypeError('from should be integer')], [from >= 0 && from < this.length, new RangeError('from is out of range')], [typeof to === 'number', new TypeError('to should be number')], [Number.isInteger(to), new TypeError('to should be integer')], [to >= 0 && to < this.length, new RangeError('to is out of range')]]);

      this.graph[from][to].addEdge(weight, data);
    }
  }, {
    key: 'dfsByEdgesNumber',
    value: function dfsByEdgesNumber(from, edgesNumber) {
      var _this = this;

      (0, _thrower2.default)([[typeof from === 'number', new TypeError('from should be number')], [Number.isInteger(from), new TypeError('from should be integer')], [from >= 0 && from < this.length, new RangeError('from is out of range')], [typeof edgesNumber === 'number', new TypeError('edgesNumber should be number')], [Number.isInteger(edgesNumber), new TypeError('edgesNumber should be integer')], [edgesNumber > 0, new RangeError('edgesNumber is out of range')]]);

      var result = {
        found: false,
        path: []
      };

      var dfs = function dfs(currentVertex, path) {
        if (path.length === edgesNumber) {
          result.found = true;
          result.path = path;
        }

        if (result.found) {
          return true;
        }

        _this.graph[currentVertex].forEach(function (vertex, vertexIndex) {
          if (vertex.hasEdges()) {
            dfs(vertexIndex, [].concat(_toConsumableArray(path), [vertex.anyEdge()]));
          }
        });
      };

      dfs(from, []);

      return result;
    }
  }, {
    key: 'dfsByWeight',
    value: function dfsByWeight(from, weight) {
      var _this2 = this;

      (0, _thrower2.default)([[typeof from === 'number', new TypeError('from should be number')], [Number.isInteger(from), new TypeError('from should be integer')], [from >= 0 && from < this.length, new RangeError('from is out of range')], [typeof weight === 'number', new TypeError('weight should be number')], [Number.isInteger(weight), new TypeError('weight should be integer')], [weight > 0, new RangeError('weight is out of range')]]);

      var result = {
        found: false,
        path: []
      };

      var dfs = function dfs(currentVertex, path) {
        var pathWeight = path.reduce(function (weightAcc, p) {
          return weightAcc + p.weight;
        }, 0);

        if (pathWeight === weight) {
          result.found = true;
          result.path = path;
        }

        if (result.found) {
          return;
        }

        _this2.graph[currentVertex].forEach(function (vertex, vertexIndex) {
          if (vertex.hasEdges()) {
            vertex.edges.forEach(function (edge) {
              if (edge.weight + pathWeight <= weight) {
                dfs(vertexIndex, [].concat(_toConsumableArray(path), [edge]));
              }
            });
          }
        });
      };

      dfs(from, []);

      return result;
    }
  }, {
    key: 'findShortestWay',
    value: function findShortestWay(from, to) {
      var _this3 = this;

      (0, _thrower2.default)([[typeof from === 'number', new TypeError('from should be number')], [Number.isInteger(from), new TypeError('from should be integer')], [from >= 0 && from < this.length, new RangeError('from is out of range')], [typeof to === 'number', new TypeError('to should be number')], [Number.isInteger(to), new TypeError('to should be integer')], [to >= 0 && to < this.length, new RangeError('to is out of range')]]);

      var vGraph = new _jsGraphAlgorithms2.default.WeightedDiGraph(this.length);

      this.graph.forEach(function (vertices, x) {
        vertices.forEach(function (vertex, y) {
          if (vertex.hasEdges()) {
            var shortestEdge = vertex.shortestEdge();

            vGraph.addEdge(new _jsGraphAlgorithms2.default.Edge(x, y, shortestEdge.weight));
          }
        });
      });

      var dijkstra = new _jsGraphAlgorithms2.default.Dijkstra(vGraph, from);

      var result = {
        found: false,
        path: []
      };

      if (dijkstra.hasPathTo(to)) {
        var shortestPath = dijkstra.pathTo(to).map(function (e) {
          var v = e.v,
              w = e.w;


          return _this3.graph[v][w].shortestEdge();
        });

        result.found = true;
        result.path = shortestPath;
      }

      return result;
    }
  }]);

  return _class;
}();

exports.default = _class;