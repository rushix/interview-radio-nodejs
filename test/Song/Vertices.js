import { expect } from 'chai';
import Vertices from '../../src/Song/Vertices';

describe('Vertices class:', () => {
  describe('corner cases and throws', () => {
    it('undefined param', () => expect(() => { new Vertices(); }).to.throw(TypeError));
    it('wrong type', () => expect(() => { new Vertices(new Set()); }).to.throw(TypeError));
    it('wrong length', () => expect(() => { new Vertices(''); }).to.throw(TypeError));
    it('restricted chars', () => expect(() => { new Vertices('% 7'); }).to.throw(Error));
    it('constructor', () => expect(() => { new Vertices('onlyoneshouldbe'); }).not.to.throw());
  });
  describe('common case', () => {
    const v = new Vertices('scottiedoesntknow2');

    const pair = { from: 28, to: 2 };
    const tag = JSON.stringify(pair);

    const fakePair = { from: 10, to: 4 };
    const fakeTag = JSON.stringify(fakePair);

    describe('have correct', () => {
      it('pair', () => {
        expect(v.pair).to.deep.equal(pair);
      });
      it('tag', () => {
        expect(v.tag).to.equal(tag);
      });
    });

    describe('try fake', () => {
      it('pair', () => {
        expect(v.pair).not.to.deep.equal(fakePair);
      });
      it('tag', () => {
        expect(v.tag).not.to.equal(fakeTag);
      });
    });

    describe('read only', () => {
      it('pair', () => {
        expect(Object.isFrozen(v.pair)).to.equal(true);
      });
      it('vertices', () => {
        expect(Object.isFrozen(v)).to.equal(true);
      });
    });

  });
});
