import { expect } from 'chai';
import Song from '../../src/Song';
import Vertices from '../../src/Song/Vertices';

describe('Song class:', () => {
  describe('corner cases and throws', () => {
    describe('wrong format', () => {
      it('undefined params', () => expect(() => { new Song(); }).to.throw(TypeError));
      it('wrong types', () => expect(() => { new Song(123, null, 29); }).to.throw(TypeError));
      it('wrong name type', () => expect(() => { new Song(false, 4536546, 44); }).to.throw(TypeError));
      it('wrong duration type', () => expect(() => { new Song('Zaika moya', new Map()); }).to.throw(TypeError));
      it('name.length < 1', () => expect(() => { new Song('', 343, 29); }).to.throw(TypeError));
      it('duration < 0', () => expect(() => { new Song('yellow tree', -227, 29); }).to.throw(TypeError));
      it('name of restricted chars', () => expect(() => { new Song(' !$@%#^#& ', 45364576, 29); }).to.throw(TypeError));
    });
    describe('correct format', () => {
      it('constructor', () => expect(() => { new Song('yesterday', 34354546, 29); }).not.to.throw());
    });
  });
  describe('common case', () => {
    const song = new Song('intro', 34435, 0);

    describe('have correct', () => {
      it('name', () => {
        expect(song.name).to.equal('intro');
      });

      it('duration', () => {
        expect(song.duration).to.equal(34435);
      });

      it('vertices', () => {
        expect(song.vertices).to.deep.equal(new Vertices('intro'));
      });

    });

    describe('try fake', () => {
      it('name', () => {
        expect(song.name).not.to.equal('outro');
      });

      it('duration', () => {
        expect(song.duration).not.to.equal(33335);
      });

      it('vertices', () => {
        expect(song.vertices).not.to.deep.equal(new Vertices('faketro'));
      });

    });

    it('is read only', () => {
      expect(Object.isFrozen(song)).to.equal(true);
    });

  });
});
