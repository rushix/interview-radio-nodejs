import { expect } from 'chai';
import thrower from '../../src/util/thrower';

describe('thrower function:', () => {
  describe('corner cases and throws', () => {
    it('wrong type', () => expect(() => { thrower(null); }).to.throw(TypeError));
    it('wrong inner type', () => expect(() => { thrower([true]); }).to.throw(TypeError));
    it('wrong cas', () => expect(() => { thrower([[new Set(), false]]); }).to.throw(TypeError));
    it('wrong err', () => expect(() => { thrower([[true, new Map()]]); }).to.throw(TypeError));
    it('constructor', () => expect(() => { thrower([[1 === 1, new SyntaxError()]]); }).not.to.throw());
  });

  describe('common cases', () => {
    const case1 = [
      [11 == '11', new Error('description')],
      [false == 0, new ReferenceError()]
    ];

    const case2 = [
      [false, new Error('very useful description')],
      [[10] === [10], new RangeError()]
    ];

    const case3 = [
      [true, new Error()],
      [2 - 2 === 0, new Error()],
      ['sw' + 'ag' === 'sw ag', new URIError('uri failure')]
    ];

    it('case1', () => {
      expect(() => { thrower(case1); }).not.to.throw();
    });

    it('case2', () => {
      expect(() => { thrower(case2); }).to.throw(Error);
    });

    it('case3', () => {
      expect(() => { thrower(case3); }).to.throw(URIError);
    });
  });
});
