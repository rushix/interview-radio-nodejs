import { expect } from 'chai';
import Node from '../../src/Graph/Node';

describe('Node class:', () => {
  describe('constructor', () => {
    it('case1', () => expect(() => { new Node(); }).not.to.throw());
    it('case2', () => expect(() => { new Node(false, [null, true]); }).not.to.throw());
  });

  describe('methods throws', () => {
    const node = new Node();

    it('addEdge1', () => expect(() => { node.addEdge(); }).to.throw(TypeError));
    it('addEdge2', () => expect(() => { node.addEdge(2.4); }).to.throw(TypeError));
    it('addEdge3', () => expect(() => { node.addEdge(-7); }).to.throw(Error));
    it('shortestEdge1', () => expect(() => { node.shortestEdge(); }).to.throw(Error));
    it('anyEdge1', () => expect(() => { node.anyEdge(); }).to.throw(Error));
    it('addEdge4', () => expect(() => { node.addEdge(14); }).not.to.throw());
    it('shortestEdge2', () => expect(() => { node.shortestEdge(); }).not.to.throw());
    it('anyEdge2', () => expect(() => { node.anyEdge(); }).not.to.throw());
  });

  describe('common case', () => {
    const node2 = new Node();

    expect(node2.hasEdges()).to.be.equal(false);

    const data = { a: 1, b: 5 };
    node2.addEdge(55, data);

    it('hasEdges2', () => {
      expect(node2.hasEdges()).to.be.equal(true);
    });

    const edge = node2.anyEdge();
    it('equality1', () => {
      expect(edge.weight).to.be.equal(55);
    });
    it('equality2', () => {
      expect(edge.data).to.be.deep.equal(data);
    });
    it('equality3', () => {
      expect(edge.data).not.to.be.deep.equal({ a: 1, b: 6 });
    });

    node2.addEdge(12);
    node2.addEdge(22);

    const shortestEdge = node2.shortestEdge();
    const anyEdge = node2.anyEdge();

    it('shortest', () => {
      expect(shortestEdge.weight).to.be.at.most(anyEdge.weight);
    });
  });
});
