import { expect } from 'chai';
import Graph from '../../src/Graph';
import Node from '../../src/Graph/Node';

describe('Graph class:', () => {
  describe('constructor', () => {
    it('case1', () => expect(() => { new Graph(); }).to.throw(TypeError));
    it('case2', () => expect(() => { new Graph(2.4); }).to.throw(TypeError));
    it('case3', () => expect(() => { new Graph(-7); }).to.throw(Error));
    it('case4', () => expect(() => { new Graph(12); }).not.to.throw());
  });

  describe('addEdge', () => {
    const graph = new Graph(7);

    it('case1', () => expect(() => { graph.addEdge(); }).to.throw(TypeError));
    it('case2', () => expect(() => { graph.addEdge(2.2, 4, 4); }).to.throw(TypeError));
    it('case3', () => expect(() => { graph.addEdge(2, 3.7, 45); }).to.throw(TypeError));
    it('case4', () => expect(() => { graph.addEdge(-100, 4, 4); }).to.throw(Error));
    it('case5', () => expect(() => { graph.addEdge(2, -5545, 45); }).to.throw(Error));
    it('case6', () => expect(() => { graph.addEdge(0, 8, 4); }).to.throw(RangeError));
    it('case7', () => expect(() => { graph.addEdge(7, 0, 45); }).to.throw(RangeError));
    it('case8', () => expect(() => { graph.addEdge(2, 1, 324, { a: 'abc' }); }).not.to.throw());

    const node = new Node();
    node.addEdge(324, { a: 'abc' });

    it('equality', () => {
      expect(graph.graph[2][1]).to.be.deep.equal(node);
    });

    const node2 = new Node();
    node2.addEdge(324, { a: 'abcd' });

    it('!equality', () => {
      expect(graph.graph[2][1]).not.to.be.deep.equal(node2);
    });
  });

  describe('dfs', () => {
    const g = new Graph(12);

    g.addEdge(0, 1, 100, 1);
    g.addEdge(1, 2, 1, 2);
    g.addEdge(2, 4, 2, 4);
    g.addEdge(4, 3, 4, 3);
    g.addEdge(4, 5, 4, 5);
    // g.addEdge(3, 1, 3, 1);

    it('throw', () => {
      expect(() => { g.dfsByEdgesNumber(); }).to.throw(TypeError);
      expect(() => { g.dfsByWeight(); }).to.throw(TypeError);
      expect(() => { g.dfsByEdgesNumber(1.2); }).to.throw(TypeError);
      expect(() => { g.dfsByWeight(3.7); }).to.throw(TypeError);
      expect(() => { g.dfsByEdgesNumber(-1); }).to.throw(RangeError);
      expect(() => { g.dfsByWeight(-50); }).to.throw(RangeError);
      expect(() => { g.dfsByEdgesNumber(0, -5); }).to.throw(RangeError);
      expect(() => { g.dfsByWeight(0, 1.3); }).to.throw(TypeError);
      expect(() => { g.dfsByEdgesNumber(0, 23); }).not.to.throw();
      expect(() => { g.dfsByWeight(0, 150); }).not.to.throw();
    });

    it('not found', () => {
      expect(g.dfsByEdgesNumber(0, 23).found).to.be.equal(false);
      expect(g.dfsByWeight(0, 150).found).to.be.equal(false);
    });

    it('found', () => {
      g.addEdge(3, 1, 3, 1);
      expect(g.dfsByEdgesNumber(0, 23).found).to.be.equal(true);
      expect(g.dfsByWeight(0, 150).found).to.be.equal(true);

      // console.log(g.dfsByEdgesNumber(0, 23));
      // console.log(g.dfsByWeight(0, 150));
      // console.log(g.findShortestWay(0, 3));
    });
  });
});
